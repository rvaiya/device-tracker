//These can be overwritten by environment variables

var CONSTANTS = {
  botname: 'devbot',
  devhost: 'http://localhost:8080',
  token: 'blarg',
  port: 8081
}

for(var k in CONSTANTS) 
  CONSTANTS[k] = process.env[k.toUpperCase()] || CONSTANTS[k];

var q = require('q');
var slack = require('../lib/slack');
var util = require('../lib/util');
var cli = require('../lib/client')(CONSTANTS.devhost);

function help() {
    var helpstr = [
        'Command: ',
        '',
        '!give [device] <user>',
        '',
        '  - Transfers ownership of device to user, if device is not available and.',
        '    the user is in possession of a single device it will transfer that device.',
        '',
        '!assign <device> <user>',
        '',
        '  - Transfers ownership of any device to a given user, in contrast to !give.',
        '    the user making the request does not need to be in possession of the device.',
        '',
        '!have <device>',
        '',
        '  - Transfers ownership of the device to the user making the request.',
        '',
        '!list',
        '',
        '  - Lists all available devices and their properties.'
    ].join("\n");
    return q('```' + helpstr + '```');
}

function scold(r) {
  var who = r.req.user_name;
  var name = r.args[0];
  var devexp = r.args[1];
  if(r.args.length !== 2) {
    return q('Usage: ' + r.trigger + 'scold <username> <device regex>');
  }
  return cli.get().then(function(devs) {
    var regexp = new RegExp(devexp);
    devs = devs.filter(function(e) {return ((e.steward === name) && e.name.match(devexp, ''));});
    if(!devs.length) {
      return q(name + ' does not have any devices which match ' + devexp);
    }
    return q(CONSTANTS.botname + ' scolds ' + name + ' for failing to track ' + devs.map(function(e) {return e.name}).join(' '));
  });
}

function claim(r) {
    if (r.args.length !== 1) return q('Usage: ' + r.trigger + 'have <device>');
    var device = r.args[0];
    var to = r.req.user_name;
    
    return cli.get().then(function(devs) {
        devs = devs.filter(function(e) {
            return e.name.match(new RegExp(device, 'i'));
        });
        if (devs.length > 1) {
            return 'Ambigous, ' + device + ' matches all of ' + devs.map(function(e) {
                return e.name
            }).join(' , ');
        }
        if(!devs.length) return 'No devices matching ' + device;

        return cli.chstew(devs[0].id, to).then(function() {
          return devs[0].name + ' transferred to ' + to;
        });
    });
}

function assign(r) {
    if (r.args.length !== 2) return q('Usage: ' + r.trigger + 'assign <device name> <user>');
    var device = r.args[0];
    var name = r.args[1].replace(/^@/, '');
    
    return cli.get().then(function(devs) {
        devs = devs.filter(function(e) {
            return e.name.match(new RegExp(device, 'i'));
        });
        if (!devs.length) return 'No device with name ' + device + ' found';
        if (devs.length > 1) return 'Ambigous, ' + device + ' matches: ' + devs.map(function(e) {
            return e.name;
        }).join(' , ');
        return cli.chstew(devs[0].id, name).then(function() {
            return name + ' is now in possession of ' + devs[0].name;
        });
    });
}

function give(r) {
    var from = r.req.user_name;
    var to = r.args[1] || r.args[0];

    var device = r.args[1] ? r.args[0] : null;

    if (!to) {
        return q('Usage: ' + r.trigger + 'give [device name] <username>');
    }

    to = to.replace(/^@/, '');
    if (!device) {
        return cli.get().then(function(devs) {
            devs = devs.filter(function(e) {
                return (e.steward === from || e.steward.match(r.req.user_id));
            });
            if (devs.length > 1) return 'Ambigous choice, ' +
                from +
                ' is in possession of all of: ' +
                devs.map(function(e) {
                    return e.name;
                }).join(' , ');
            if (devs.length < 1) return from + ' is not currently in posession of any devices';
            return cli.chstew(devs[0].id, to).then(function() {
                return devs[0].name + ' successfully transferred to ' + to;
            });
        });
    } else {
        return cli.get().then(function(devs) {
            devs = devs.filter(function(e) {
                return e.name.match(new RegExp(device, 'i'));
            });
            if (devs.length > 1) return 'Ambigous choice, potential candidates ' + devs.map(function(e) {
                return e.name;
            }).join(' , ');
            if (devs.length < 1) return device + ' does not match any device names';
            if (devs[0].steward !== from && !devs[0].steward.match(r.req.user_id)) return from + ' is not currently in possession of ' +
                                                      devs[0].name + ' (try ' + r.trigger + 'assign)';
            return cli.chstew(devs[0].id, to).then(function() {
                return devs[0].name + ' successfully transferred to ' + to;
            });
        });
    }
}

function list() {
    var fields = ['name', 'description', 'os', 'steward'];
    var header = ['name', 'description', 'operating system', 'location'];
return cli.get().then(function(devs) {
        var payload = [header].concat(devs.map(function(e) {
            e.steward = '<@' + e.steward.replace(/(^<|>$|@)/g, '') + '>';
            return fields.map(function(k) {
                return e[k];
            });
        }));
        return '```' + util.prettyArray(payload) + '```';
    });
}

function ping() {
    return q('pong');
}

var handlers = {
    give: give,
    list: list,
    assign: assign,
    have: claim,
    ping: ping,
    scold: scold,
    help: help
};

var app = slack(handlers, CONSTANTS.botname, CONSTANTS.token);

app.listen(CONSTANTS.port, function() {
  console.log('Listening on port ' + CONSTANTS.port);
});
