#!/usr/bin/env node

var createServ = require('../server/main');

var PORT = process.env.PORT || 8080;

var mongo = require('mongodb').MongoClient;
mongo.connect('mongodb://localhost:27017/devices',
  function (err, db) {
    if (err) throw err;
    var app = createServ(db);
    app.listen(PORT, function () {
      console.log('listening on port ' + PORT);
    });
  });
