exports.JSONMiddleware = function (req, res, n) {
  if(req.method === 'GET' || req.method === 'DELETE' ) {
    n();
    return;
  }
  var data = '';
  req.on('data', function(c) { data+=c; });
  req.on('end', function() { 
    try {
      req.body = JSON.parse(data);
    } catch(e) {
      res.json({
        msg: 'INVALID_JSON',
        status: 419
      })
      return;
    }
      n();
  });
};

exports.prettyArray = function (arrs, maxchar) {
  var result = "";
  //Discover maxchar for each column
  if(!maxchar) {
    var maxchars = [];
    for(var i=0;i<arrs.length;i++) {
      for(var j=0;j<arrs[i].length;j++) {
        maxchars[j] = maxchars[j] || 0;
        maxchars[j] = (arrs[i][j].toString().length > maxchars[j]) ? arrs[i][j].toString().length : maxchars[j];
      }
    }
  }

  arrs.forEach(function(arr) {
    for(var i=0;i<arr.length;i++) {
      var str = arr[i];
      var lim = maxchar || maxchars[i];
      str = str.substr(0, lim);

      var nsp = (lim-str.length);
      for(var j=0;j<nsp;j++) 
        str += " ";
      
      result += str + "  ";
    }
    result += "\n";
  });

  return result;
};
