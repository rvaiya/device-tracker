var request = require('superagent');
var q = require('q');

//Client library for the device server

function createCli(DEV_SERVER) {
  function del(dev) {
    var p = q.defer();
    request
      .del(DEV_SERVER + '/device/' + dev)
      .end(function(err, r) {
        if(err || r.status !== 200) {
          p.reject(err || r.body);
          return;
        }
        p.resolve(r.body);
      });
    return p.promise;
  }

  function add(dev) {
    var p = q.defer();
    request
      .put(DEV_SERVER + '/device')
      .send(dev)
      .end(function(err, r) {
        if(err || r.status !== 200) {
          p.reject(err || r.body);
          return;
        }
        p.resolve(r.body);
      });
    return p.promise;
  }

  function get() {
    var p = q.defer();
    request
      .get(DEV_SERVER + '/devices')
      .end(function(err, r) {
        if(err || r.status !== 200) {
          p.reject(err || r.body);
          return;
        }
        p.resolve(r.body.data);
      });
    return p.promise;
  }

  function chown(id, user) {
    var p = q.defer();
    request
      .post(DEV_SERVER + '/device/' + id)
      .send({
        owner: user
      })
      .end(function(err, r) { 
        if(err || r.status !== 200) {
          p.reject(err || r.body);
          return;
        }
        p.resolve(r.body);
      });
    return p.promise;
  }

  function chstew(id, user) {
    var p = q.defer();
    request
      .post(DEV_SERVER + '/device/' + id)
      .send({
        steward: user
      })
      .end(function(err, r) { 
        if(err || r.status !== 200) {
          p.reject(err || r.body);
          return;
        }
        p.resolve(r.body);
      });
    return p.promise;
  }

  return {
    chstew: chstew,
    chown: chown,
    delete: del,
    get: get,
    add: add
  };
}

module.exports = createCli;
