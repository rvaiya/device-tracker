angular.module('main')
.controller('main', function (api, constants, globals) {
  var scope = this;

  scope.removeDevice = function(id) {
    api.removeDevice(id).then(globals.updateDevs, function(e) {
      window.alert('Failed to delete device ' + e);
    });
  };
  //Device properties to show
  scope.devkeys = [ 
    'name',
    'description',
    'os',
    'uuid',
    'serial',
    'owner',
    'steward'
  ];
});
