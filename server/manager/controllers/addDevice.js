angular.module('main')
.controller('addDevice', function (globals, api, $window) {
  var scope = this;
  scope.device = {};
  
  api.getSchema().then(function dialogReady(r) {
      scope.deviceSchema = Object.keys(r);
      scope.showDialog = true;
  });

  scope.addDevice = function(device) {
    api.getDevices().then(function(devs) {
      var match = devs.filter(function(d) {
        return (device.name === d.name);
      });
      if(match.length) 
        throw new Error('Device with the given name already exists, refusing to post');
      return device;
    })
    .then(api.addDevice)
    .then(function() {
      $window.alert('Successfully added ' + device.name);
    }, function(e) {
      $window.alert((e.data && e.data.msg) || e.message);
    }); 
  };
})
