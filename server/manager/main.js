angular.module('main', ['ui.router'])
  .config(function($stateProvider) {
    $stateProvider.state('main', {
      url: '',
      templateUrl: 'main.html',
    });
    $stateProvider.state('addDevice', {
      url: '/addDevice',
      templateUrl: 'partials/addDevice.html',
    });
  })
  .service('constants', function () {
    this.devUrl = '';
    this.pollInterval = 5000;
  })
  .service('api', function ($http, constants) {
    function genUpdater(url) {
      return function() {
        return $http.get(url).then(function (r) {
          return r.data.data;
        });
      };
    }

    this.removeDevice = function(id) {
      console.log(id);
      return $http.delete(constants.devUrl + '/device/' + id);
    };

    this.addDevice = function(dev) {
      return $http.put(constants.devUrl + '/device', dev);
    };

    this.getSchema = genUpdater(constants.devUrl + '/deviceSchema');
    this.getDevices = genUpdater(constants.devUrl + '/devices');
  })
  .service('globals', function (api, constants, $interval, $rootScope) {
    var scope = this;

    this.updateDevs = function() {
      api.getDevices().then(function(devs) {
        scope.devices = devs;
      });
    };

    this.updateSchema = function() {
      api.getSchema().then(function(schema) {
        scope.schema = schema;
      });
    };

    this.updateDevs();
    $interval(this.updateDevs, constants.pollInterval);
  })
  .run(function($rootScope, globals) {
    $rootScope.globals = globals;
  });
