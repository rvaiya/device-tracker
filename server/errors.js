//HTTP RESPONSE CODES [FIX**]

module.exports = {
  SUCCESS: {
    status: 200,
    msg: 'Success',
  },
  INVALID_JSON: {
    status: 404,
    msg: 'INVALID_JSON',
  },
  INVALID_QUERY: {
    status: 419,
    msg: 'INVALID_QUERY'
  },
  NOT_FOUND: {
    status: 404,
    msg: 'NOT_FOUND'
  },
  INVALID_DEVICE: {
    status: 419,
    msg: 'INVALID_DEVICE'
  },
  DEVICE_EXISTS: {
    status: 409,
    msg: 'DEVICE_EXISTS'
  },
  DEVICE_ID_IN_USE: {
    status: 409,
    msg: 'DEVICE_ID_IN_USE'
  },
  EMPTY_POST_REQUEST: {
    status: 419,
    msg: 'EMPTY_POST_REQUEST'
  },
  INVALID_OBJECT_ID: {
    status: 419,
    msg: 'INVALID_OBJECT_ID'
  }
};
