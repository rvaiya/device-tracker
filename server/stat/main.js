angular.module('main', ['ui.router'])
.service('constants', function () {
  this.devUrl = '';
  this.pollInterval = 5000;
})
.controller('main', function ($interval, $http, constants) {
    var scope = this;
    function updateDeviceList() {
      $http.get(constants.devUrl + '/devices').then(function (r) {
        scope.devices = r.data.data;
      });
    }
    updateDeviceList();

    scope.devkeys = [ 
      'name',
      'description',
      'os',
      'uuid',
      'serial',
      'owner',
      'steward'
    ];
});
