var ObjectId = require('mongodb').ObjectID;
var ec = require('./errors');
var q = require('q');
var util = require('../lib/util');
var express = require('express');
//REST api for device tracker

//Device validation
var deviceSchema = {
  'name': 'string',
  'description': 'string',
  'uuid': 'string',
  'serial': 'string',
  'os': 'string',
  'owner': 'string', //E.g rangle/nick
  'steward': 'string'
};
  
function validDevice(dev) {
  if(Object.keys(dev).length !== Object.keys(deviceSchema).length) return false;
  for(var k in dev) {
    if(deviceSchema[k] !== typeof dev[k]) return false;
  }

  return true;
}

function validPartDevice(dev) {
  if(Object.keys(dev).length > Object.keys(deviceSchema).length) return false;
  for(var k in dev) {
    if(deviceSchema[k] !== typeof dev[k]) return false;
  }

  return true;
}


//Util

function senderr(res, ec) {
  ec.data = null;
  res.status(ec.status).json(ec);
}

function sendjson(res, obj) {
  ec.SUCCESS.data = obj || null;
  res.json(ec.SUCCESS);
}

//Main

function createApp(db) {
  var devlist = db.collection('devlist');
  var app = express();

  app.use(function(req, res, n) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    n();
  });

  var stamps = {};
  
  app.use('/stat', express.static(__dirname + '/stat'));
  app.use('/manager', express.static(__dirname + '/manager'));
  app.use(util.JSONMiddleware);

  app.get('/deviceSchema', function(req, res) {
    sendjson(res, deviceSchema);
  });

  app.get('/devices', function(req, res) {
    devlist.find().toArray(function(err, data) {
      if(err) throw err;

      data.forEach(function(e) {
        e.id=e._id;
        delete e._id;
      });

      sendjson(res, data);
    });
  });

  app.put('/device', function(req, res) {
    var dev = req.body;
    if (!validDevice(dev)) {
      senderr(res, ec.INVALID_DEVICE);
      return;
    }

    devlist.insert(dev, function(err, r) {
      if(err) throw err;

      devlist.find().toArray(function(err, r) {
        senderr(res, ec.SUCCESS);
      });
    });
  });

  app.route('/device/:id')
    .all(function(req, res, n) {
      try {
        req.oid = new ObjectId(req.params.id);
      } catch(e) {
        req.oid = req.params.id;
      }

      n();
    })
    .post(function(req, res) {
      if(!validPartDevice(req.body)) {
        senderr(res, ec.INVALID_QUERY);
        return;
      }

      if(Object.keys(req.body).length === 0) {
        senderr(res, ec.EMPTY_POST_REQUEST);
        return;
      }

      var stamp = stamps[req.oid];

      if(req.body.steward && req.body.steward !== stamp.steward) {
        var session = { user: req.body.steward, start: stamp.ts, end: new Date() };
        devlist.update({_id: req.oid}, { $set: req.body, $addToSet: { sessions: session }}, function(err, r) { 
          if(err) throw err;
          if(!r.result.n) {
            senderr(res, ec.NOT_FOUND);
            return;
          }

          stamp.ts = new Date();
          stamp.steward = req.body.steward;
          senderr(res, ec.SUCCESS);
        });
      } else {
        devlist.update({_id: req.oid}, { $set: req.body }, function(err, r) { 
          if(err) throw err;
          if(!r.result.n) {
            senderr(res, ec.NOT_FOUND);
            return;
          }

          senderr(res, ec.SUCCESS);
        });
    }
  }) 
    .get(function(req, res) {
      devlist.findOne({_id: req.oid}, function(err, item) {
        if(err || !item) {
          senderr(res, ec.NOT_FOUND);
          return;
        }

        item.id = item._id;
        delete item._id;
        sendjson(res, item);
      });
    })
    .delete(function(req, res) {
      devlist.deleteOne({ _id: req.oid }, function(err, r) {
        if(err) throw err;
        if(!r.result.n) {
          senderr(res, ec.NOT_FOUND);
          return;
        }
        senderr(res, ec.SUCCESS);
      });
    });
  
  (function() {
    var o = app.listen;
    app.listen = function() {
      var oargs = Array.prototype.slice.call(arguments);
      devlist.find().toArray(function(err, data) {
        if(err) throw err;
        data.forEach(function(e) {
            stamps[e._id] = {
              steward: e.steward, 
              ts: new Date()
            };
          }); 
          o.apply(app, Array.prototype.slice.call(oargs));
      });
    }
  })();

  return app;
}

module.exports = createApp;
