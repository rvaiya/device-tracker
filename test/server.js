var TEST_DB = 'mongodb://localhost:27017/testdb';

var q = require('q');
var expect = require('chai').expect;
var request = require('supertest');
var MongoClient = require('mongodb').MongoClient;
var createApp = require('../server/main');
var errors = require('../server/errors');
var app = null;

var badDevice = {
  name: 'name',
  uuid: 'uuid',
  serial: 'serial'
};

var goodDevice = {
  name: 'name',
  description: 'description',
  uuid: 'uuid',
  serial: 'serial',
  owner: 'owner',
  steward: 'steward',
  os: 'os'
};

function getDevices() {
  var p = q.defer();
  request(app)
    .get('/devices')
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function (err, r) {
      if (err) {
        p.reject(err);
        return
      }
      p.resolve(r.body.data);
    });
  return p.promise;
}

function expecterr(err, done) {
  return function(e, res) {
    if(e) throw e;
    err.data = null;
    expect(res.body).to.deep.equal(err);
    expect(res.status).to.be.equal(err.status);
    done();
  }
}

before(function (done) {
  MongoClient.connect(TEST_DB, function (err, db) {
    if (err) throw err;

    db.collection('devlist').drop(function () {
      app = createApp(db);
      done();
    });
  });
});


describe('server', function () {
  describe('/devices', function () {
    it('should return an array', function () {
      return getDevices().then(function(r) {
        expect(typeof r).to.be.equal('object');
      });
    });
  });
  describe('/device', function () {
    describe('PUT', function () {
      it('should reject a bad device', function (done) {
        request(app)
          .put('/device')
          .send(badDevice)
          .end(expecterr(errors.INVALID_DEVICE, done));
      });

      it('should accept a valid device', function (done) {
        request(app)
          .put('/device')
          .send(goodDevice)
          .expect(200)
          .end(function(err, r) {
            if(err) throw err;
            getDevices().then(function (devs) {
              expect(devs.length).to.be.equal(1);
              expect(devs[0].name).to.be.equal(goodDevice.name);
              expect(devs[0].id).not.to.be.undefined();
              done();
            }).then(null, done);
          });
      });
    });

    describe('GET', function () {
      it('should return the appropriate device given a specific device id', function(done) {
        getDevices().then(function(devs) {
          request(app)
            .get('/device/' + devs[0].id)
            .expect(200)
            .end(function(err, r) {
              if(err) throw err;
              expect(devs[0]).to.deep.equal(r.body.data);
              done();
            });
        }).then(null, done);
      });
    });

    describe('POST', function (done) {
      it('should complain when given an empty POST request', function (done) {
        request(app)
          .post('/device/nonexistent')
          .send({})
          .end(expecterr(errors.EMPTY_POST_REQUEST, done));
      });

      it('should reject a request with an invalid device ID', function (done) {
        request(app)
          .post('/device/nonexistent')
          .send({
            name: 'newname'
          })
         .end(expecterr(errors.NOT_FOUND, done));
      });

      it('should reject a POST request with an invalid payload', function (done) {
        getDevices().then(function(devs) {
          request(app)
            .post('/device/' + devs[0].id)
            .send({
              name: 'newname',
              id: 'newid' //Invalid param, should be rejected
            })
            .end(expecterr(errors.INVALID_QUERY, done));
        }).then(null, done);
      });

      it ('should accept valid POST request', function (done) {
        getDevices().then(function (devs) {
          expect(devs[0]).not.to.be.undefined();
          expect(devs[0].id).not.to.be.undefined();
          request(app)
            .post('/device/' + devs[0].id)
            .send({
              name: 'newname'
            })
            .expect(200, done);
        }).then(null, done);
      });

      it ('should reflect the changes made in the POST request', function () {
        return getDevices().then(function(devs) {
          var mDevice = goodDevice;
          mDevice.name = 'newname';
          mDevice.id = devs[0].id;
          expect(devs[0]).to.deep.equal(mDevice);
        });
      });
    });

    describe('DELETE', function (done) {
      it('should accept a valid DELETE requests', function(done) {
        getDevices().then(function(devs) {
          request(app)
            .delete('/device/' + devs[0].id)
            .expect(200, done);
        }).then(null, done);
      });

      it('should manifest in the devices list', function() {
        return getDevices().then(function(devs) {
          expect(devs.length).to.be.equal(0);
        });
      });

      it('should reject invalid DELETE requests', function(done) {
        request(app)
          .delete('/device/nonexistent')
          .end(expecterr(errors.NOT_FOUND, done));
      });
    });
  });
});
