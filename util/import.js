#!/usr/bin/env node

//Addes a list of devices in a json file to the specified server

var cli = require('../lib/client')(process.env.DEVICE_HOST || 'localhost:8080');
var fs = require('fs');

function die(e) { console.error(e.stack); process.exit(1); }

var devs = JSON.parse(fs.readFileSync(process.argv[3]).toString());

devs.forEach(function(dev)  {
  cli.add(dev).then(function(e) {
    console.log('Successfully added ' + dev.name);
  }, die);
});

