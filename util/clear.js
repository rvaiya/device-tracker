var client = require('../lib/client.js')(process.env.DEVICE_HOST || 'http://localhost:8080');
var q = require('q');

var clean = function() {
  return client.get().then(function(devs) {
    return q.all(devs.map(function(e) {return client.delete(e.id);}));
  });
};

clean().then(function() {
  var fname = process.argv[2];
  var devs = JSON.parse(require('fs').readFileSync(process.argv[2]));
  return q.all(devs.map(function(dev) { return client.add(dev); }));
}).then(null, console.error);
